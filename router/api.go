package router

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func NewRouterApi(app *gin.Engine, dbConnection *gorm.DB) {
	// // Init All Repository
	// userRepository := repositories.NewUserRepository(dbConnection)

	// // Init All Service
	// userService := services.NewUserService(userRepository)

	// // Init All Controller
	// userController := controllers.NewUserController(userService)

	// // Handler All Routes
	// apiRoute := app.Group("/api")
	// apiRoute.Use(middleware.CorsMiddleware()) // GLobal Middleware

	// apiRoute.GET("/users", userController.Index)
	// // apiRoute.GET("/users", middleware.AuthMiddleware(), userController.Index) // single middleware
}
