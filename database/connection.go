package database

import (
	"gitlab.com/punyasaya19/go-starter-template/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func GetDatabaseConnection(conf config.Config) *gorm.DB {
	// Connect Database
	dsn := conf.DB.User + ":" + conf.DB.Pass + "@tcp(" + conf.DB.Host + ":" + conf.DB.Port + ")/" + conf.DB.Name + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db = db.Debug()

	// Return
	return db
}
