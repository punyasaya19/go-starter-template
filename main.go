package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/punyasaya19/go-starter-template/config"
	"gitlab.com/punyasaya19/go-starter-template/database"
	"gitlab.com/punyasaya19/go-starter-template/router"
)

func main() {
	// Call config
	cnfg := config.Get()

	// Call database connection
	dbConnection := database.GetDatabaseConnection(cnfg)

	// Initial Router GIN
	r := gin.Default()
	// r := gin.New() // pure config server

	// Init API Route
	router.NewRouterApi(r, dbConnection)

	// Run Server
	r.Run(cnfg.Srv.Host + ":" + cnfg.Srv.Port)
}
