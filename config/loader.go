package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	DB      Database
	Srv     Server
	Session Session
}

func Get() Config {
	// Load .env
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Failed when load .env %s", err.Error())
	}

	return Config{
		Srv: Server{
			Host: os.Getenv("SRV_HOST"),
			Port: os.Getenv("SRV_PORT"),
		},
		DB: Database{
			Host: os.Getenv("DB_HOST"),
			Port: os.Getenv("DB_PORT"),
			User: os.Getenv("DB_USERNAME"),
			Pass: os.Getenv("DB_PASSWORD"),
			Name: os.Getenv("DB_DATABASE"),
		},
		Session: Session{
			SESSION_ID: os.Getenv("SESSION_ID"),
		},
	}
}
