package config

type Server struct {
	Host string
	Port string
}
