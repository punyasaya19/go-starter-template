package pkg

import (
	"github.com/gorilla/sessions"
	"gitlab.com/punyasaya19/go-starter-template/config"
)

var StoreSession = sessions.NewCookieStore([]byte(config.Get().Session.SESSION_ID))
