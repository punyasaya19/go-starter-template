package pkg

type ResponseJson struct {
	Code   int
	Status bool
	Msg    string
	Data   any
	Error  string
	Errors any
}
